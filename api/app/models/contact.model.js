module.exports = (sequelize, Sequelize) => {
  const Contact = sequelize.define("contacts", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      notNull: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.STRING,
      notNull: true,
    },
    email: {
      type: Sequelize.STRING,
    },
    phoneNumber: {
      type: Sequelize.STRING,
    },
    createdAt: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    },
    updatedAt: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
  });

  return Contact;
};
