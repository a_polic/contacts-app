module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("users", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      notNull: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.STRING,
      notNull: true,
    },
    email: {
      type: Sequelize.STRING,
      notNull: true,
    },
    password: {
      type: Sequelize.STRING,
      notNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    },
    updatedAt: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
  });

  return User;
};
