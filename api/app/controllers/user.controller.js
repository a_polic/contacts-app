const config = require("../config/auth.config.js");
const db = require("../models");
const Contact = db.contact;

var jwt = require("jsonwebtoken");
const { contact } = require("../models");

exports.getContacts = (req, res) => {
  let token = req.headers["x-access-token"];
  let decoded = jwt.decode(token);

  Contact.findAll({
    where: {
      userId: decoded.id,
    },
  })
    .then((contacts) => {
      return res.status(200).send({
        message: "User Contacts.",
        contacts: contacts,
      });
    })
    .catch((err) => {
      res.status(500).send({ message: err.message });
    });
};

exports.postContact = (req, res) => {
  let token = req.headers["x-access-token"];
  let decoded = jwt.decode(token);
  Contact.create({
    name: req.body.name,
    email: req.body.email,
    phoneNumber: req.body.phoneNumber,
    userId: decoded.id,
  })
    .then((contact) => {
      res.send({
        message: "Contact was registered successfully!",
        contact: contact,
      });
    })
    .catch((err) => {
      res.status(500).send({ message: err.message });
    });
};

exports.deleteContact = (req, res) => {
  let contact = req.params.id;
  Contact.destroy({
    where: {
      id: contact,
    },
  })
    .then(() => {
      res.send({
        message: "Contact was deleted successfully!",
      });
    })
    .catch((err) => {
      res.status(500).send({ message: err.message });
    });
};
