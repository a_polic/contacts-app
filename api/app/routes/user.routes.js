const { authJwt } = require("../middleware");
const controller = require("../controllers/user.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/contacts", [authJwt.verifyToken], controller.getContacts);

  app.post("/api/contact", [authJwt.verifyToken], controller.postContact);

  app.delete(
    "/api/contact/:id",
    [authJwt.verifyToken],
    controller.deleteContact
  );
};
